package com.itechart.queryperformanceproblem.repository.impl;

import com.itechart.queryperformanceproblem.repository.BenchmarkRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static java.util.stream.Collectors.toList;

@Repository
public class BenchmarkRepositoryImpl implements BenchmarkRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(BenchmarkRepositoryImpl.class);

    private List<DatabaseLock> databaseLocks;

    public BenchmarkRepositoryImpl(List<JdbcTemplate> jdbcTemplates) {
        databaseLocks = jdbcTemplates.stream()
                .map(DatabaseLock::new)
                .collect(toList());
    }

    @Override
    public List<Integer> execute(String query) {
        return databaseLocks.parallelStream().map(databaseLock -> {
            databaseLock.lock.lock();
            try {
                LOGGER.info("Started execution of query \"{}\"", query);
                long start = System.nanoTime();
                databaseLock.jdbcTemplate.execute(query);
                int spentTime = (int) ((System.nanoTime() - start) / 1000_000);
                LOGGER.info("Finished execution of query \"{}\" with spent time = {}", query, spentTime);
                return spentTime;
            } finally {
                databaseLock.lock.unlock();
            }
        }).collect(toList());
    }

    private class DatabaseLock {

        private Lock lock;
        private JdbcTemplate jdbcTemplate;

        DatabaseLock(JdbcTemplate jdbcTemplate) {
            this.jdbcTemplate = jdbcTemplate;
            this.lock = new ReentrantLock();
        }
    }

}
