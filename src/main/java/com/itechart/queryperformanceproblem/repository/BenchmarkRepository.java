package com.itechart.queryperformanceproblem.repository;

import java.util.List;

/**
 * Class for measuring of a SQL-query performance against each configured
 * database.
 */
public interface BenchmarkRepository {

    /**
     * The method accepts a SQL-query string and runs it across all databases
     * of the application with recording spent time. Only one call will be
     * executed at the same time for each of databases.
     *
     * @param query SQL-query for which benchmark should be generated.
     * @return A list of spent times for the query to be executed against each
     * database in milliseconds.
     */
    List<Integer> execute(String query);
}
