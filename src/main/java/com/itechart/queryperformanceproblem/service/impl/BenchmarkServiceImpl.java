package com.itechart.queryperformanceproblem.service.impl;

import com.itechart.queryperformanceproblem.domain.QueryBenchmark;
import com.itechart.queryperformanceproblem.repository.BenchmarkRepository;
import com.itechart.queryperformanceproblem.service.BenchmarkService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BenchmarkServiceImpl implements BenchmarkService {

    private static final String NO_DATABASES_FOUND = "No databases found.";

    private BenchmarkRepository benchmarkRepository;

    public BenchmarkServiceImpl(BenchmarkRepository benchmarkRepository) {
        this.benchmarkRepository = benchmarkRepository;
    }

    @Override
    public QueryBenchmark testQuery(String query) {
        List<Integer> timings = benchmarkRepository.execute(query);
        return new QueryBenchmark(timings, timings.stream().mapToInt(Integer::intValue).average()
                .orElseThrow(() -> new IllegalStateException(NO_DATABASES_FOUND)));
    }

}
