package com.itechart.queryperformanceproblem.service;

import com.itechart.queryperformanceproblem.domain.QueryBenchmark;

/**
 * Service for creating performance benchmarks for SQL-queries.
 */
public interface BenchmarkService {

    /**
     * The method accepts a SQL-query string and runs it across all databases
     * of the application with recording spent time and returning average as
     * well. Only one call will be executed at the same time for each of
     * databases. The list of timings isn't ordered.
     *
     * @param query SQL-query for which benchmark should be generated.
     * @return An object with information about performance benchmarks of the
     * query: times for each database to execute the query and calculated
     * average of those timings.
     */
    QueryBenchmark testQuery(String query);

}
