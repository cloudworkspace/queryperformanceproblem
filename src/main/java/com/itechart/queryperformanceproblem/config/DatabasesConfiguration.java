package com.itechart.queryperformanceproblem.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;

/**
 * Class for declaration database related beans. All declared JdbcTemplate
 * beans will be used by the application for measuring benchmarks for
 * SQL-queries.
 */
@Configuration
public class DatabasesConfiguration {

    @Primary
    @Bean(name = "dataSourceForDatabaseA")
    @ConfigurationProperties(prefix = "database_a.datasource")
    public DataSource dataSourceForDatabaseA() {
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "dataSourceForDatabaseB")
    @ConfigurationProperties(prefix = "database_b.datasource")
    public DataSource dataSourceForDatabaseB() {
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "jdbcTemplateDatabaseA")
    public JdbcTemplate jdbcTemplateDatabaseA(@Qualifier("dataSourceForDatabaseA") DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }

    @Bean(name = "jdbcTemplateDatabaseB")
    public JdbcTemplate jdbcTemplateDatabaseB(@Qualifier("dataSourceForDatabaseB") DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }
}
