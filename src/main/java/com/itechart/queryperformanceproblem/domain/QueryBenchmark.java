package com.itechart.queryperformanceproblem.domain;

import java.util.List;

/**
 * Class for representation of performance benchmark information about a
 * SQL-query: times to execute for each of databases in milliseconds and
 * average of those timings.
 */
public class QueryBenchmark {

    private List<Integer> timings;

    private double average;

    public QueryBenchmark(List<Integer> timings, double average) {
        this.timings = timings;
        this.average = average;
    }

    public List<Integer> getTimings() {
        return timings;
    }

    public double getAverage() {
        return average;
    }
}
