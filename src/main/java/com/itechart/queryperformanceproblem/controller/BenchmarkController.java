package com.itechart.queryperformanceproblem.controller;

import com.itechart.queryperformanceproblem.domain.QueryBenchmark;
import com.itechart.queryperformanceproblem.service.BenchmarkService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.jdbc.BadSqlGrammarException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * The controller is an entry point for REST API for performance benchmarking
 * SQL-queries.
 */
@RestController
public class BenchmarkController {

    private static final Logger LOGGER = LoggerFactory.getLogger(BenchmarkController.class);

    private BenchmarkService benchmarkService;

    public BenchmarkController(BenchmarkService benchmarkService) {
        this.benchmarkService = benchmarkService;
    }

    /**
     * The method is used as a representation of GET-request for REST API, it
     * should receive a single parameter - a SQL-query string with name "query"
     * and return JSON object with performance benchmarks of the query: spent
     * time for the query to be executed for each of databases and average of
     * them.
     *
     * @param query SQL-query string.
     * @return An object with information about performance benchmarks of the
     * query: times for each database to execute the query and average of them
     * (in milliseconds).
     */
    @GetMapping(path = "/test-query")
    public QueryBenchmark testQuery(@RequestParam("query") String query) {
        LOGGER.info("Query received for getting benchmark: \"{}\"", query);
        return benchmarkService.testQuery(query);
    }

    @ExceptionHandler(BadSqlGrammarException.class)
    public void databaseError(BadSqlGrammarException exception, HttpServletResponse response) throws IOException {
        response.sendError(HttpStatus.BAD_REQUEST.value(), exception.getMessage());
    }

}
