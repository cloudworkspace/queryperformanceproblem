package com.itechart.queryperformanceproblem.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.IntStream;

import static java.lang.Thread.sleep;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doAnswer;

@RunWith(SpringRunner.class)
@WebMvcTest(BenchmarkRepository.class)
public class BenchmarkRepositoryTest {

    private static final String TEST_QUERY = "SELECT * FROM test_table";
    private static final int SLEEP_MILLISECONDS = 10;
    private static final int NUMBER_OF_JOBS_TO_EXECUTE_IN_PARALLEL = 20;

    @Autowired
    private BenchmarkRepository benchmarkRepository;

    @MockBean(name = "jdbcTemplateDatabaseA")
    private JdbcTemplate jdbcTemplateDatabaseA;

    @Test
    public void testCalculateTimingsForAllDeclaredJdbcTemplates() {
        assertThat(benchmarkRepository.execute(TEST_QUERY).size()).isEqualTo(1);
    }

    @Test
    public void testNotTryToExecuteMoreThanOneQueryToOneDatabaseInParallel() throws InterruptedException {
        ReentrantLock lock = new ReentrantLock();
        AtomicInteger amountOfAvailableLocks = new AtomicInteger(0);
        doAnswer(invocation -> {
            if (lock.tryLock()) {
                // The counter will be increased only when "execute" method is executing one in parallel.
                amountOfAvailableLocks.incrementAndGet();
            }
            sleep(SLEEP_MILLISECONDS);
            if (lock.isHeldByCurrentThread()) {
                lock.unlock();
            }
            return null;
        }).when(jdbcTemplateDatabaseA).execute(TEST_QUERY);
        ExecutorService executorService = Executors.newFixedThreadPool(NUMBER_OF_JOBS_TO_EXECUTE_IN_PARALLEL);
        IntStream.range(0, NUMBER_OF_JOBS_TO_EXECUTE_IN_PARALLEL)
                .forEach(i -> executorService.submit(() -> benchmarkRepository.execute(TEST_QUERY)));
        executorService.shutdown();
        executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);

        assertThat(amountOfAvailableLocks.get()).isEqualTo(NUMBER_OF_JOBS_TO_EXECUTE_IN_PARALLEL);
    }

}
