package com.itechart.queryperformanceproblem.controller;

import com.itechart.queryperformanceproblem.domain.QueryBenchmark;
import com.itechart.queryperformanceproblem.service.BenchmarkService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.jdbc.BadSqlGrammarException;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@WebMvcTest(BenchmarkController.class)
public class BenchmarkControllerTest {

    private static final String TEST_QUERY = "SELECT * FROM test_table";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private BenchmarkService benchmarkService;

    @Test
    public void testReturnAverageFromService() throws Exception {
        when(benchmarkService.testQuery(TEST_QUERY))
                .thenReturn(new QueryBenchmark(Arrays.asList(1, 2, 3), 2));
        this.mockMvc.perform(get("/test-query?query=" + TEST_QUERY))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.average").value(2.0));
    }

    @Test
    public void testReturnTimingsFromService() throws Exception {
        when(benchmarkService.testQuery(TEST_QUERY))
                .thenReturn(new QueryBenchmark(Arrays.asList(1, 2, 3), 2));
        this.mockMvc.perform(get("/test-query?query=" + TEST_QUERY))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.timings").isArray())
                .andExpect(jsonPath("$.timings", containsInAnyOrder(1, 2, 3)));
    }

    @Test
    public void testReturnBadRequestForBadSyntax() throws Exception {
        when(benchmarkService.testQuery(TEST_QUERY)).thenThrow((BadSqlGrammarException.class));
        this.mockMvc.perform(get("/test-query?query=" + TEST_QUERY))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testReturnBadRequestForMissingQuery() throws Exception {
        this.mockMvc.perform(get("/test-query"))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }
}
