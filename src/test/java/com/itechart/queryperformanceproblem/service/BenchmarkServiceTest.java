package com.itechart.queryperformanceproblem.service;

import com.itechart.queryperformanceproblem.repository.BenchmarkRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@WebMvcTest(BenchmarkService.class)
public class BenchmarkServiceTest {

    private static final String TEST_QUERY = "SELECT * FROM test_table";

    @Autowired
    private BenchmarkService benchmarkService;

    @MockBean
    private BenchmarkRepository benchmarkRepository;

    @Test
    public void testCalculateAverage() {
        when(benchmarkRepository.execute(TEST_QUERY))
                .thenReturn(Arrays.asList(1, 2, 3));
        assertThat(benchmarkService.testQuery(TEST_QUERY).getAverage()).isEqualTo(2);
    }

    @Test
    public void testReturnTimingsFromRepository() {
        when(benchmarkRepository.execute(TEST_QUERY))
                .thenReturn(Arrays.asList(1, 2, 3));
        assertThat(benchmarkService.testQuery(TEST_QUERY).getTimings()).containsExactlyInAnyOrder(1, 2, 3);
    }
}
